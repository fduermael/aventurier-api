# aventurier-api project

## Design

The design of this API project is based on Domain Driven Design :
- an independent core,
- and dependencies.

### Core
A core should avoid all dependencies and footprints of technical frameworks.

It contains :
- the domain with non-anemic classes, 
- the interfaces of the repositories without implementation, 
- the services.

It is why we have not used quarkus injection dependencies in core classes, but singleton design patterns.

### Dependencies
They are core dependent : 
- Implementations for data persistence. 
- Resources allowing resources to be exposed in API form, with REST architecture style.

Such a conception  allows greater maintainability. 

For now, data persistence is in memory, but it would be effortlessly to add NoSQL persistence, as with MongoDB.

The implementation choices are performed at the resource level. 

These dependencies can be framework (REST and injection dependency) or persistence tool dependant.  

DAs same as the core, which is a component, each dependency should be a separated component, provided in a specific JAR.

### Business rules
- Added features at early conception : Management of several players with continuation of game play
- Detection of map borders
- Detection of collisions with natural obstacles or other players 

The decision of business rule that has been done is the following :
- if a move is impossible (out of borders, collision), the character stays at the same position.  


### Main classes of the domain
Naming is done in English, avoiding java language reuse

- AdventureMap
- AdventureCharacter
- Adventure

Implementation is non-anemic, following the Domain Driven Design recommandations. 

## Technical implementation

This project requires Java 11 and uses Quarkus, the Supersonic Subatomic Java Framework.

The skeleton of the code has been automatically generated with https://code.quarkus.io

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .


### RESTEasy JAX-RS

The API is exposed with RESTEasy JAX-RS. 
Guide: https://quarkus.io/guides/rest-json

The endpoints are as follows :
```shell script 
http://localhost:8080/resteasy/adventure (operations: POST, PUT, GET)
http://localhost:8080/resteasy/adventurecharacter (operations: POST, PUT)
```

Whenever it is possible, the identifiers (adventure, adventurecharacter) are in the resource path, following the conventional way.

### Continuous Integration
Continuous Integration is conditioned by the acceptance of unit tests.

These unit tests are made on the persistence dependency and on the resource dependency, with Jupiter JUnit 5, enabled by Quarkus test annotations.

### Client
The chosen client is Postman. It provides a way to test interactively the API, by sending REST requests, and displaying their result. 

Collection Postman v2.1 available in JSON under src/main/resources/Aventurier.postman_collection.json

Here is the scenario :

- Creation of an aventure (id:1)
```shell script
 POST http://0.0.0.0:8080/resteasy/adventure
```
- Creation of two players (id:1 et 2)
```shell script
POST http://0.0.0.0:8080/resteasy/adventurecharacter?symbol=1&initialPosX=3&initialPosY=0
POST http://0.0.0.0:8080/resteasy/adventurecharacter?symbol=2&initialPosX=9&initialPosY=6
```
- Move of these two players (1 et 2) for this adventure (1)
```shell script
PUT http://0.0.0.0:8080/resteasy/adventure/1/1?moves=SSSSEEEEEENN
PUT http://0.0.0.0:8080/resteasy/adventure/1/2?moves=OONOOOSSO
```
- Display of the adventure 1
```shell script
GET http://0.0.0.0:8080/resteasy/adventure/1/map
```


## Project management with Maven

### Running the application in dev mode 

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

### Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `aventurier-api-1.0.0-SNAPSHOT-runner.jar` file in the `/target` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/lib` directory.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application is now runnable using `java -jar target/aventurier-api-1.0.0-SNAPSHOT-runner.jar`.

### Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/aventurier-api-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.html.
