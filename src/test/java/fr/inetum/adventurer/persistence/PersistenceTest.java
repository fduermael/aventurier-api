package fr.inetum.adventurer.persistence;

import fr.inetum.adventurer.core.domain.AdventureMap;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import org.wildfly.common.Assert;

@QuarkusTest
public class PersistenceTest {

    @Test
    public void loadTest() {

        AdventureMap adventureMap = new FileAdventureMapRepository().load();
        Assert.assertTrue(adventureMap.getLines().size()==20);
    }

    @Test
    public void loadTest2() {

        AdventureMap adventureMap = new FileSystemAdventureMapRepository().load();
        Assert.assertTrue(adventureMap.getLines().size()==20);
    }
}
