package fr.inetum.adventurer.resource;

import fr.inetum.adventurer.core.domain.Adventure;
import fr.inetum.adventurer.core.domain.AdventureCharacter;
import fr.inetum.adventurer.core.service.AdventureCharacterService;
import fr.inetum.adventurer.core.service.AdventureService;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import org.wildfly.common.Assert;

import javax.inject.Inject;
import java.util.logging.Logger;

import static io.restassured.RestAssured.given;


@QuarkusTest
public class AdventureResourceTest {

    /**
     * dependency injection
     */

    @Inject
    AdventureService adventureService;

    @Inject
    AdventureCharacterService adventureCharacterService;



    @Test
    public void testCreateAdventure() {
        given()
                .when().post("/resteasy/adventure")
                .then()
                .statusCode(200);
        ;
    }

    @Test
    public void testAdventure1Endpoint() {
        Adventure adventure =  adventureService.createAdventure();
        Integer adventureId =  adventureService.persist(adventure);
        AdventureCharacter adventureCharacter = adventureCharacterService.createAdventureCharacter("1", 3, 0);
        Integer adventureCharacterId = adventureCharacterService.persist(adventureCharacter);
        given()
          .when().put("/resteasy/adventure/"+adventureId+"/"+adventureCharacterId+"?moves=SSSSEEEEEENN")
          .then()
             .statusCode(204);
        Logger.getLogger("io.quarkus").info("testAdventure1Endpoint:"+adventureCharacter.getCurrentPosition().toString());
        String adventureMapString = adventure.getAdventureMap().toString();
        Logger.getLogger("io.quarkus").info("testAdventure1Endpoint:\n"+adventureMapString);
        Assert.assertTrue(adventureCharacter.getCurrentPosition().getX()==9);
        Assert.assertTrue(adventureCharacter.getCurrentPosition().getY()==2);

    }

    @Test
    public void testAdventure2Endpoint() {
        Adventure adventure =  adventureService.createAdventure();
        Integer adventureId =  adventureService.persist(adventure);
        AdventureCharacter adventureCharacter = adventureCharacterService.createAdventureCharacter("2", 9, 6);
        Integer adventureCharacterId = adventureCharacterService.persist(adventureCharacter);
        given()
                .when().put("/resteasy/adventure/"+adventureId+"/"+adventureCharacterId+"?moves=OONOOOSSO")
                .then()
                .statusCode(204);
        Logger.getLogger("io.quarkus").info("testAdventure2Endpoint:"+adventureCharacter.getCurrentPosition().toString());
        String adventureMapString = adventure.getAdventureMap().toString();
        Logger.getLogger("io.quarkus").info("testAdventure2Endpoint:\n"+adventureMapString);
        Assert.assertTrue(adventureCharacter.getCurrentPosition().getX()==5);
        Assert.assertTrue(adventureCharacter.getCurrentPosition().getY()==7);

    }

    @Test
    public void testAdventureMapEndpoint() {
        Adventure adventure =  adventureService.createAdventure();
        String adventureMapString = adventure.getAdventureMap().toString();
        Logger.getLogger("io.quarkus").info("testAdventureMapEndpoint:\n"+adventureMapString);
        Integer adventureId =  adventureService.persist(adventure);
        given()
                .when().get("/resteasy/adventure/"+adventureId+"/map")
                .then()
                .statusCode(200);
                //.body(is(adventureMapString));

    }

}