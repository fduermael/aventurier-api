package fr.inetum.adventurer.resource;


import fr.inetum.adventurer.core.domain.AdventureCharacter;
import fr.inetum.adventurer.core.service.AdventureCharacterService;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/resteasy/adventurecharacter")
public class AdventureCharacterResource {
    /**
     * dependency injection
     */

    @Inject
    AdventureCharacterService adventureCharacterService;


    /**
     *
     * @param adventureCharacterSymbol
     * @param initialPosX
     * @param initialPosY
     * @return persisted adventure character id
     */
    @POST()
    @Produces(MediaType.TEXT_PLAIN)
    public int createAdventureCharacter(@QueryParam("symbol")String adventureCharacterSymbol, @QueryParam("initialPosX") int initialPosX, @QueryParam("initialPosY") int initialPosY) {
        AdventureCharacter adventureCharacter = adventureCharacterService.createAdventureCharacter(adventureCharacterSymbol, initialPosX, initialPosY);
        return adventureCharacterService.persist(adventureCharacter);
    }


    /**
     * reset character to its initial position
     * @param adventureCharacterId
     */
    @Path("/{adventureCharacterId}")
    @PUT()
    public void resetAdventureCharacter(@PathParam("adventureCharacterId")Integer adventureCharacterId) {
        AdventureCharacter adventureCharacter = adventureCharacterService.findById(adventureCharacterId);
        adventureCharacter.reset();
    }
}
