package fr.inetum.adventurer.resource;

import fr.inetum.adventurer.core.domain.Adventure;
import fr.inetum.adventurer.core.domain.AdventureCharacter;
import fr.inetum.adventurer.core.domain.Position;
import fr.inetum.adventurer.core.service.AdventureCharacterService;
import fr.inetum.adventurer.core.service.AdventureService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Optional;


@Path("/resteasy/adventure")
public class AdventureResource {

    /**
     * dependency injection
     */

    @Inject
    AdventureService adventureService;

    @Inject
    AdventureCharacterService adventureCharacterService;


    /**
     *
     * @return adventure id
     */
    @POST()
    @Produces(MediaType.TEXT_PLAIN)
    public int createAdventure() {
        Adventure adventure =  adventureService.createAdventure();
        return adventureService.persist(adventure);
    }

    /**
     *
     * @param adventureCharacterId
     * @param initialPosX
     * @param initialPosY
     * @param sequenceOfMoves
     */
    @Path("/{adventureId}/{adventureCharacterId}")
    @PUT()
    public void playMoveCommand(@PathParam("adventureId") int adventureId, @PathParam("adventureCharacterId")  int adventureCharacterId,
                                @DefaultValue("-1") @QueryParam("initialPosX") int initialPosX, @DefaultValue("-1")  @QueryParam("initialPosY") int initialPosY, @QueryParam("moves")String sequenceOfMoves) {
        Adventure adventure = adventureService.findById(adventureId);
        AdventureCharacter adventureCharacter = adventureCharacterService.findById(adventureCharacterId);

        // no initial position
        if (initialPosX == -1 && initialPosY== -1) {
            adventureService.playMoveCommand(adventure, adventureCharacter, Optional.empty(), sequenceOfMoves);
        }
        else {
            adventureService.playMoveCommand(adventure, adventureCharacter, Optional.of(new Position(initialPosX,initialPosY)), sequenceOfMoves);
        }


    }

    /**
     *
     * @return map content string representation filled with character player symbols
     */
    @Path("/{adventureId}/map")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String findAdventureMap(@PathParam("adventureId") int adventureId) {
        Adventure adventure = adventureService.findById(adventureId);
        return adventure.getAdventureMap().toString();
    }
}
