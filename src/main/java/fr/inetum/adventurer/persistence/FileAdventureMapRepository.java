package fr.inetum.adventurer.persistence;

import fr.inetum.adventurer.core.domain.AdventureMap;
import fr.inetum.adventurer.core.domain.Line;
import fr.inetum.adventurer.core.repository.AdventureMapRepository;

import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

//@ApplicationScoped
public class FileAdventureMapRepository implements AdventureMapRepository {
    @Override
    public AdventureMap load() {
        String fileName = "src/main/resources/META-INF/resources/carte.txt";

        AdventureMap adventureMap = new AdventureMap();

        //read file into stream, try-with-resources
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

            stream.forEach(s -> adventureMap.addLine(new Line(s)));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return adventureMap;
    }
}
