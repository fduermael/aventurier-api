package fr.inetum.adventurer.persistence;

import fr.inetum.adventurer.core.domain.Adventure;
import fr.inetum.adventurer.core.repository.AdventureRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
public class InMemoryAdventureRepository implements AdventureRepository {


    private static Integer counter = 1;

    // storage per id
    private Map<Integer, Adventure> idMap = new HashMap<>();


    synchronized public Integer persist(Adventure adventure) {
        adventure.setId(counter);
        counter++;
        idMap.put(adventure.getId(), adventure);
        return adventure.getId();
    }



    public Adventure findById(Integer id) {
        return idMap.get(id);
    }

}
