package fr.inetum.adventurer.persistence;

import fr.inetum.adventurer.core.domain.AdventureMap;
import fr.inetum.adventurer.core.domain.Line;
import fr.inetum.adventurer.core.repository.AdventureMapRepository;

import javax.enterprise.context.ApplicationScoped;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

@ApplicationScoped
public class FileSystemAdventureMapRepository implements AdventureMapRepository {
    @Override
    public AdventureMap load() {
        String fileName = "carte.txt";

        AdventureMap adventureMap = new AdventureMap();

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
        if (inputStream == null) {
            throw new RuntimeException(fileName+" not found");
        }
        // read map line by line in old school way
        try (InputStreamReader streamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
             BufferedReader reader = new BufferedReader(streamReader)) {

            String line;
            while ((line = reader.readLine()) != null) {
                adventureMap.addLine(new Line(line));
            }


        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return adventureMap;
    }
}
