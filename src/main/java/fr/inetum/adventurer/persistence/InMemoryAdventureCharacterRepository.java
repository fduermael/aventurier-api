package fr.inetum.adventurer.persistence;

import fr.inetum.adventurer.core.domain.AdventureCharacter;
import fr.inetum.adventurer.core.repository.AdventureCharacterRepository;
import fr.inetum.adventurer.core.repository.AdventureRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
public class InMemoryAdventureCharacterRepository implements AdventureCharacterRepository {


    private static Integer counter = 1;

    // storage per id
    private Map<Integer, AdventureCharacter> idMap = new HashMap<>();

    synchronized public Integer persist(AdventureCharacter adventureCharacter) {
        adventureCharacter.setId(counter);
        counter++;
        idMap.put(adventureCharacter.getId(), adventureCharacter);
        return adventureCharacter.getId();
    }

    public AdventureCharacter findById(Integer id) {
        return idMap.get(id);
    }

}
