package fr.inetum.adventurer.core.repository;

import fr.inetum.adventurer.core.domain.Adventure;

public interface AdventureRepository {

    Integer persist(Adventure adventure);

    Adventure findById(Integer id);
}
