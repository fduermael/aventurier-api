package fr.inetum.adventurer.core.repository;

import fr.inetum.adventurer.core.domain.AdventureCharacter;

public interface AdventureCharacterRepository {

    Integer persist(AdventureCharacter character);

    AdventureCharacter findById(Integer id);
}
