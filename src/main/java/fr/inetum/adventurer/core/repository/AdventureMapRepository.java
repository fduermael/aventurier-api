package fr.inetum.adventurer.core.repository;

import fr.inetum.adventurer.core.domain.AdventureMap;

public interface AdventureMapRepository {

    AdventureMap load();
}
