package fr.inetum.adventurer.core.domain;

public class AdventureCharacter {

    /**
     * id in repository
     */

    private Integer id;

    /**
     * symbol of adventure character in the map
     */
    private String symbol;

    /**
     * initial position
     */
    private Position initialPosition;

    /**
     * sequence of moves from initial position
     */
    private String history;

    /**
     * current position
     */
    private Position currentPosition;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Position getInitialPosition() {
        return initialPosition;
    }

    public void setInitialPosition(Position initialPosition) {
        this.initialPosition = initialPosition;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public Position getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(Position currentPosition) {
        this.currentPosition = currentPosition;
    }

    /**
     * computes new position from current one without any constraint
     * @param direction
     * @return new Position
     */
    public Position go(Direction direction) {
        Position newPosition = new Position(currentPosition);
        switch (direction) {
            case O:
                newPosition.setX(newPosition.getX()-1);
                break;
            case E:
                newPosition.setX(newPosition.getX()+1);
                break;
            case N:
                newPosition.setY(newPosition.getY()-1);
                break;
            case S:
                newPosition.setY(newPosition.getY()+1);
                break;
        }
        return newPosition;
    }

    /**
     * initialization of position with initial one
     */
    public void reset() {

        currentPosition = new Position(initialPosition);
        history = "";

    }
}
