package fr.inetum.adventurer.core.domain;

import fr.inetum.adventurer.core.exception.ImpossibleMoveException;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Adventure {


    /**
     * identifier
     */
    private Integer id;

    /**
     * characters (players) located in the map
     */
    private List<AdventureCharacter> adventureCharacters = new ArrayList();

    /**
     * map used in the adventure
     */
    private AdventureMap adventureMap;


    public List<AdventureCharacter> getAdventureCharacters() {
        return adventureCharacters;
    }

    public void setAdventureCharacters(List<AdventureCharacter> adventureCharacters) {
        this.adventureCharacters = adventureCharacters;
    }

    public AdventureMap getAdventureMap() {
        return adventureMap;
    }

    public void setAdventureMap(AdventureMap adventureMap) {
        this.adventureMap = adventureMap;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    /**
     * update current position of a character following a move towards a direction
     * @param adventureCharacter
     * @param direction
     * @throws ImpossibleMoveException
     */
    public void go(AdventureCharacter adventureCharacter, Direction direction) throws ImpossibleMoveException {
        Position oldPosition = adventureCharacter.getCurrentPosition();
        Position newPosition = adventureCharacter.go(direction);
        // control boundaries
        if (newPosition.getX()<0 || newPosition.getY()<0) {
                throw new ImpossibleMoveException(newPosition);
        }
        if (newPosition.getY()>adventureMap.getLines().size()-1) {
            throw new ImpossibleMoveException(newPosition);
        }
        Line newLine = adventureMap.getLines().get(newPosition.getY());
        if (newPosition.getX()>newLine.length() -1) {
            throw new ImpossibleMoveException(newPosition);
        }
        // change position of character
        if (newLine.isFree(newPosition.getX())) {
            adventureCharacter.setCurrentPosition(newPosition);
            // perform changes on map
            adventureMap.removeAtPosition(oldPosition);
            adventureMap.addAdventureCharacter(adventureCharacter);
        }
        else {
            throw new ImpossibleMoveException(newPosition);
        }
    }

    public void move(AdventureCharacter adventureCharacter, String sequenceOfMoves)  {

        char[] charArray = sequenceOfMoves.toCharArray();
        for (char c : charArray) {
            Direction direction = Direction.valueOf(""+c);
            try {
                go(adventureCharacter, direction);
                // add to history when possible move
                adventureCharacter.setHistory(adventureCharacter.getHistory()+c);
            }
            catch (ImpossibleMoveException e) {
                Logger.getLogger("io.quarkus").info("Impossible move at "+e.getPosition().toString());
            }

        }

    }


}
