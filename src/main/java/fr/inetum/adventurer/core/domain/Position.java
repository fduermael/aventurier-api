package fr.inetum.adventurer.core.domain;

/**
 * coordonates
 */
public class Position {

    /**
     * constructor
     * @param p
     */

    public Position(Position p) {
        x = p.x;
        y = p.y;
    }

    /**
     *
     * @param x
     * @param y
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * x coordonate (horizontal)
     */
    private int x;

    /**
     * y coordonate (vertical)
     */
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String toString() {
        return "("+x+","+y+")";
    }

}
