package fr.inetum.adventurer.core.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Adventure map
 */
public class AdventureMap {

    /**
     * a map is composed of lines from up to bottom
     */
    private List<Line> lines = new ArrayList();

    /**
     * get all the lines of a map
     */
    public List<Line> getLines() {
        return lines;
    }

    /**
     * add a line at the bottom
     */
    public void addLine(Line line) {
        lines.add(line);
    }

    /**
     * add a character in the map
     * @param adventureCharacter
     */
    public void addAdventureCharacter(AdventureCharacter adventureCharacter) {
        Position currentPosition = adventureCharacter.getCurrentPosition();
        Line currentLine = lines.get(currentPosition.getY());
        char[] currentCharArray = currentLine.getContent().toCharArray();
        String adventureCharacterSymbol = adventureCharacter.getSymbol();
        if (adventureCharacterSymbol == null || adventureCharacterSymbol.isEmpty()) {
            adventureCharacterSymbol = new String(new char[]{Line.PLAYER_ONE});
        }
        currentCharArray[currentPosition.getX()] = adventureCharacterSymbol.charAt(0);
        currentLine.setContent(new String(currentCharArray));
    }

    /**
     * remove a character in the map
     * @param currentPosition
     */
    public void removeAtPosition(Position currentPosition) {

        Line currentLine = lines.get(currentPosition.getY());
        char[] currentCharArray = currentLine.getContent().toCharArray();
        currentCharArray[currentPosition.getX()] = Line.EMPTY;
        currentLine.setContent(new String(currentCharArray));
    }


    /**
     * remove a character in the map
     * @param adventureCharacter
     */
    public void removeAdventureCharacter(AdventureCharacter adventureCharacter) {
        Position currentPosition = adventureCharacter.getCurrentPosition();
        removeAtPosition(currentPosition);
    }



    public String toString() {
        return lines.stream()
                .map(line -> line.getContent())
                .collect(Collectors.joining("\n"));

    }

}
