package fr.inetum.adventurer.core.domain;

public enum Direction {

    /**
     * cardinal points
     */

    O("O"),
    E("E"),
    N("N"),
    S("S");

    /**
     * symbol in map move
     */
    private String name;

    Direction(String name) {
        this.name = name;
    }

    public String symbol() {
        return name;
    }


}
