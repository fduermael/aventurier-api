package fr.inetum.adventurer.core.domain;

public class Line {


    /**
     * empty character so that the emplacement is free
     */
    public static char EMPTY = ' ';

    /**
     * first player by default
     */

    public static char  PLAYER_ONE = '1';

    /**
     * content compound of free spaces and obstacles
     */
    private String content;


    public Line(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    /**
     * check ArrayOutOfBoundException
     * @param i
     * @return
     */
    public Character getCharAt(int i) {
        return content.toCharArray()[i];

    }

    public Boolean isFree(int i) {
        return EMPTY == getCharAt(i);
    }

    public int length() {
        return content.length();
    }
}
