package fr.inetum.adventurer.core.exception;


import fr.inetum.adventurer.core.domain.Position;

/**
 * risen when obstacle reached during move into a direction
 */
public class ImpossibleMoveException extends RuntimeException {

    public ImpossibleMoveException(Position position) {
        this.position = position;
    }

    /**
     * position of the obstacle
     */
    Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
