package fr.inetum.adventurer.core.service;

import fr.inetum.adventurer.core.domain.Adventure;
import fr.inetum.adventurer.core.domain.AdventureCharacter;
import fr.inetum.adventurer.core.domain.Position;
import fr.inetum.adventurer.core.repository.AdventureMapRepository;
import fr.inetum.adventurer.core.repository.AdventureRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.*;

/**
 * in a pure way of design, no framework dependency in core
 */
@ApplicationScoped
public class AdventureService {

    @Inject
    AdventureRepository adventureRepository;

    @Inject
    AdventureMapRepository adventureMapRepository;


    /**
     * create an empty adventure
     * @return adventure
     */
    public Adventure createAdventure() {
        var adventure = new Adventure();
        var adventureMap =  adventureMapRepository.load();
        adventure.setAdventureMap(adventureMap);
        return adventure;
    }

    /**
     * add a player command
     * @param adventure
     * @param adventureCharacter
     * @param initialPosition ex (0,0) optional (not provided in a turn, only at beginning)
     * @param sequenceOfMoves ex "ESN"
     */
    public void playMoveCommand(Adventure adventure, AdventureCharacter adventureCharacter, Optional<Position> initialPosition, String sequenceOfMoves) {
        // if not already present, we add it
        var adventureCharacterSet = new HashSet<>(adventure.getAdventureCharacters());
        if (!adventureCharacterSet.contains(adventureCharacter)) {
            adventure.getAdventureCharacters().add(adventureCharacter);
        }
        // initial position at start
        if (initialPosition.isPresent()) {
            adventureCharacter.setInitialPosition(initialPosition.get());
            adventureCharacter.reset();
        }
        adventure.move(adventureCharacter,sequenceOfMoves);
    }

    /**
     * encapsulation of repository
     * @param adventure
     * @return
     */
    public Integer persist(Adventure adventure) {
        return adventureRepository.persist(adventure);
    }

    /**
     *encapsulation of repository
     * @param id
     * @return
     */
    public Adventure findById(Integer id) {
        return adventureRepository.findById(id);
    }

}
