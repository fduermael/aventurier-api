package fr.inetum.adventurer.core.service;

import fr.inetum.adventurer.core.domain.AdventureCharacter;
import fr.inetum.adventurer.core.domain.Position;
import fr.inetum.adventurer.core.repository.AdventureCharacterRepository;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;


/**
 * in a pure way of design, no framework dependency in core
 */
@ApplicationScoped
public class AdventureCharacterService {


    @Inject
    AdventureCharacterRepository adventureCharacterRepository;


    /**
     * create an empty adventure
     * @return adventure
     */
    public AdventureCharacter createAdventureCharacter(String adventureCharacterSymbol, int initialPosX, int initialPosY) {
        var adventureCharacter = new AdventureCharacter();
        adventureCharacter.setSymbol(adventureCharacterSymbol);
        adventureCharacter.setInitialPosition(new Position(initialPosX,initialPosY));
        adventureCharacter.reset();
        return adventureCharacter;
    }

    /**
     * encapsulation of service
     * @param adventureCharacter
     * @return
     */
    public Integer persist(AdventureCharacter adventureCharacter) {
        return adventureCharacterRepository.persist(adventureCharacter);
    }

    /**
     * encapsulation of repository
     * @param id
     * @return
     */
    public AdventureCharacter findById(Integer id) {
        return adventureCharacterRepository.findById(id);
    }

}
